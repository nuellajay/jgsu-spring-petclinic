FROM openjdk:8-jre-alpine
VOLUME /tmp 
ADD /target/*.jar spring-petclinic-*.jar 
CMD ["/usr/bin/java", "-jar", "./spring-petclinic-*.jar"]
